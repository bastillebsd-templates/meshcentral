# Meshcentral
Bastille template to bootstrap Meshcentral

## Status
[![pipeline status](https://gitlab.com/bastillebsd-templates/meshcentral/badges/master/pipeline.svg)](https://gitlab.com/bastillebsd-templates/meshcentral/commits/master)

## Bootstrap
```shell
bastille bootstrap https://gitlab.com/bastillebsd-templates/meshcentral
```

## Usage
```shell
bastille template TARGET bastillebsd-templates/meshcentral
